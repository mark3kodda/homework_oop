package main.com.oophomework;

import main.com.oophomework.models.Device;
import main.com.oophomework.models.Memory;
import main.com.oophomework.models.ProcessorArm;
import main.com.oophomework.models.ProcessorX86;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        ProcessorArm proc1 = new ProcessorArm(5000,10,32);
        ProcessorX86 proc2 = new ProcessorX86(7000,20, 14);
        ProcessorArm proc3 = new ProcessorArm(5000,10,3);
        ProcessorX86 proc4 = new ProcessorX86(7000,21, 4);
        ProcessorArm proc5 = new ProcessorArm(5000,23,2);
        ProcessorX86 proc6 = new ProcessorX86(8000,60, 64);
        ProcessorArm proc7 = new ProcessorArm(400,10,13);
        ProcessorX86 proc8 = new ProcessorX86(2500,52, 16);
        ProcessorArm proc9 = new ProcessorArm(3000,40,28);
        ProcessorX86 proc10 = new ProcessorX86(12000,20, 64);

        int memoryToGive1 = 5;
        int memoryToGive2 = 7;
        int memoryToGive3 = 4;
        int memoryToGive4 = 12;
        int memoryToGive5 = 3;

        Memory memory1 = new Memory(memoryToGive1);
        Memory memory2 = new Memory(memoryToGive2);
        Memory memory3 = new Memory(memoryToGive3);
        Memory memory4 = new Memory(memoryToGive4);
        Memory memory5 = new Memory(memoryToGive5);


        Device device1 = new Device(proc1,memory1);
        Device device2 = new Device(proc2,memory2);
        Device device3 = new Device(proc3,memory4);
        Device device4 = new Device(proc4,memory5);
        Device device5 = new Device(proc5,memory4);
        Device device6 = new Device(proc6,memory3);
        Device device7 = new Device(proc7,memory2);
        Device device8 = new Device(proc8,memory3);
        Device device9 = new Device(proc9,memory5);
        Device device10 = new Device(proc10,memory1);

        String[] dataForDevice1 = {"glor","One","One","nulle","mor"};
        String[] dataForDevice10 = {"g","g","One","g","One"};
        String[] dataForDevice2 = {"Two","g","One","g","soup","g","tree"};
        String[] dataForDevice7 = {"ss","lorem","IPSUM","g","soup","g","g"};
        String[] dataForDevice3 = {"ss","ob","ob","g","soup","g","g","ob","ob","IPSUM","g","soup"};
        String[] dataForDevice5 = {"NON","NON","none","nine","soup","g","asdas","vzxc","ob","IPSUM","g","soup"};
        String[] dataForDevice4 = {"pok","pok","pok"};
        String[] dataForDevice9 = {"Par","Par","g"};
        String[] dataForDevice6 = {"Par","Par","g","g"};
        String[] dataForDevice8 = {"oak","oar","g","oink"};
        // пишет верх ногами и это правильно
        // записать в последнюю ячейку, значение которой null, вернуть true, если свободных ячеек памяти нет – вернуть false;

        //итог заполняй целиком, а потом удаляй/освобождай нужное кол-во ячеек


        device1.save(dataForDevice1);
        System.out.println(memory1.getMemoryInfo());

        memory1.removeLast();
        device1.getSystemInfo();
        System.out.println(memory1.getMemoryInfo());

        System.out.println("The length is "+memory1.memoryCell.length);



        System.out.println(Arrays.toString(device1.readAll()));
        //System.out.println(Arrays.toString(memory1.memoryCell)+" this is memoryCell1 after saving");



//        device9.save(dataForDevice9);
//        device4.save(dataForDevice4);
//        device3.save(dataForDevice3);
//        device5.save(dataForDevice5);
//        device2.save(dataForDevice2);
//        device10.save(dataForDevice10);
//        device7.save(dataForDevice7);
//        device6.save(dataForDevice6);
//        device8.save(dataForDevice8);

        //System.out.println(device10.getSystemInfo());

    }
}
