package main.com.oophomework.models;

public class ProcessorX86 extends Processor{

    public static final  String ARCHITECTURE = "X86";

    public ProcessorX86(int frequency, int cache, int bitCapacity) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    public String dataProcess(String data) {
        if(data == null){
            return null;
        }
        return getDetails().toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        return String.valueOf(data / 100);
    }
}
