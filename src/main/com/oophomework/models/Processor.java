package main.com.oophomework.models;

public abstract class Processor {

   private int frequency;
   private int cache;
   private int bitCapacity;

    public Processor(int frequency, int cache, int bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    public String getDetails(){
        return "frequency " + frequency + ", cache " + cache + ", bitCapacity " + bitCapacity+".";
    }

    abstract public String dataProcess(String data);
    abstract public String dataProcess(long data);

}
