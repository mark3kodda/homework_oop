package main.com.oophomework.models;

import java.util.Objects;

public class MemoryInfo {

    public int memorySize;
    public double freePercentage;

    public MemoryInfo(int memorySize, double freePercentage) {
        this.memorySize = memorySize;
        this.freePercentage = freePercentage;
    }

    public int getMemorySize() {
        return memorySize;
    }

    public double getFreePercentage() {
        return freePercentage;
    }

    @Override
    public String toString() {
        return "memoryInfo{" +
                "memorySize=" + memorySize +
                ", freePercentage=" + freePercentage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemoryInfo that = (MemoryInfo) o;
        return memorySize == that.memorySize &&
                freePercentage == that.freePercentage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(memorySize, freePercentage);
    }
}
