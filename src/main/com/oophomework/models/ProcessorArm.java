package main.com.oophomework.models;

public class ProcessorArm extends Processor {

    public static final  String ARCHITECTURE = "ARM";

    public ProcessorArm(int frequency, int cache, int bitCapacity) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    public String dataProcess(String data) {
        if(data == null){
            return null;
        }
        return data.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {
        return String.valueOf(data / 100);

    }
}
