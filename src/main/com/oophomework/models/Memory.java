package main.com.oophomework.models;

public class Memory {

    public String[] memoryCell;

    public Memory(int length){
        this.memoryCell = new String[length];
    }


    //вычитать значение из последнего записанного элемента массива
    // (значение которого не null),
    public String readLast(){
        String result = null;
        if(memoryCell.length==0){
            result = "Memory has 0 capacity";
        }else{
        for (int i = memoryCell.length-1; i > 0 ; i--) {
            if(memoryCell[i]!=null){
                result = memoryCell[i];
                break;
            }
        }
        if(result == null){
            result = "Memory is empty. readLast";
        }

        }
        return result;
    }
    //удаление/возвращение последнего элемента (записать значение null),
    public String removeLast(){
        String result = null;
        if(memoryCell.length == 0){
            result = "Memory has 0 capacity";
        }else{
        for (int i = memoryCell.length-1 ; i > 0 ; i--) {
            if(memoryCell[i] != null){
                result = memoryCell[i];
                memoryCell[i] = null;
                break;
            }
        }
        if(result == null){
            result = "Memory is empty. removeLast";
        }
        }
        return result;
    }

    //записать в последнюю ячейку, значение которой null, вернуть true,
    // если свободных ячеек памяти нет – вернуть false;

    //если будешь пробовать ложить null изначально, то он перезатрётся

    public boolean save(String value){ //берём value и ложим в массив Мемори
        for (int i = memoryCell.length-1 ; i >= 0 ; i--) {
            if(memoryCell[i]==null){
            memoryCell[i] = value;
                return true;}
        }
        return false;
    }
//общий объём памяти (количество доступных ячеек), занятый объём памяти (в процентах).
    public MemoryInfo getMemoryInfo() {

        if(memoryCell.length==0){
            return new MemoryInfo(0,0);
        }

        int counter = 0;
        for (int i = 0; i <memoryCell.length ; i++){
            if (memoryCell[i] == null) {
                counter++;
            }
        }
        double occupiedMemoryPercentage = counter * 100 / memoryCell.length;
        return new MemoryInfo (memoryCell.length,occupiedMemoryPercentage);
    }
}

