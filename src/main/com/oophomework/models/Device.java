package main.com.oophomework.models;

import java.util.Arrays;

public class Device {

    private Processor processor;
    private Memory memory;

    public Device(Processor processor, Memory memory) {
        this.processor = processor;
        this.memory = memory;
    }

    public void save (String[] data){

        for (int i = 0; i <data.length ; i++) {
            boolean cellFilled = memory.save(data[i]);
            if(!cellFilled){
                break;
            }
        }
        //System.out.println(Arrays.toString(memory.memoryCell));

    }

    //вычитка всех элементов из памяти, затем стирание данных
    public String[] readAll(){

        if(memory.memoryCell.length==0){
            System.out.println("Memory has 0 capacity");
            return null;
        }

        int nullsCounter = 0;
        for (int i = 0; i <memory.memoryCell.length ; i++) {
            if(memory.memoryCell[i]==null){
                nullsCounter++;
            }
        }
        String[] storedData = new String[memory.memoryCell.length-nullsCounter]; // как узнать размер ИЛИ как
        int counter=0;
        //грубо но можно просто продублировать массив и просто перегнать
        String[] buffer = memory.memoryCell;
        for (int i = 0; i <buffer.length ; i++) {
            if(!(buffer[i] ==(null))){
                storedData[counter]=buffer[i];
                counter++;
            }
        }
        return storedData;
    }


    public void dataProcessing(){
        //преобразование всех данных, записанных в памяти
        String[] buffer = readAll();
        for(int i = 0; i < buffer.length; i++){
            buffer[i] = processor.dataProcess(buffer[i]);
        }
        save(buffer);

    }


    public String getSystemInfo(){
        //получение строки с информацией о системе (информация о процессоре, памяти)
        return processor.getDetails().concat("\n"+memory.getMemoryInfo().toString());
    }
}
