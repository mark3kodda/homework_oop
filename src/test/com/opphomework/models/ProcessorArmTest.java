package test.com.opphomework.models;

import main.com.oophomework.models.ProcessorArm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ProcessorArmTest {

    private static final ProcessorArm cut = new ProcessorArm(5000,10,32);

    static Arguments[] dataProcessTestArgs() {
        return new Arguments[]{
                Arguments.arguments("loko","LOKO" ),
                Arguments.arguments("asdf", "ASDF"),
                Arguments.arguments("", ""),
                Arguments.arguments(" fm ", " FM "),
                Arguments.arguments("  ", "  "),
                Arguments.arguments(" -1", " -1"),
                Arguments.arguments(null, null),
        };
    }
    @ParameterizedTest
    @MethodSource("dataProcessTestArgs")
    void dataProcess(String condition,String expected){
        String actual = cut.dataProcess(condition);
        Assertions.assertEquals(expected,actual);
    }
}