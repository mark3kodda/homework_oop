package test.com.opphomework.models;

import main.com.oophomework.models.Memory;
import main.com.oophomework.models.MemoryInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class MemoryTest {

    private static final Memory cut = new Memory(5);


    static private String[] firstCase = {"one", "two", "three", "four", "five"};
    static private String[] secondCase = {null, "one", "two", "three", null};
    static private String[] thirdCase = {null, null, "one", "two"};
    static private String[] fourthCase ={};
    static private String[] fifthCase ={null, null, null, null, null};

    static Arguments[] readLastTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"one", "two", "three", "four", "five"}, "five"),
                Arguments.arguments(new String[]{null, "one", "two", "three", null}, "three"),
                Arguments.arguments(new String[]{null, null, "one", "two"}, "two"),
                Arguments.arguments(new String[]{}, "Memory has 0 capacity"),
                Arguments.arguments(new String[]{null, null, null, null, null}, "Memory is empty. readLast"),

        };
    }

    @ParameterizedTest
    @MethodSource("readLastTestArgs")
    void readLastTest(String[]condition,String expected) {
        cut.memoryCell=condition;
        String actual = cut.readLast();
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] removeLastTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"one", "two", "three", "four", "five"}, "five"),
                Arguments.arguments(new String[]{null, "one", "two", "three", null}, "three"),
                Arguments.arguments(new String[]{null, null, "one", "two"}, "two"),
                Arguments.arguments(new String[]{}, "Memory has 0 capacity"),
                Arguments.arguments(new String[]{null, null, null, null, null}, "Memory is empty. removeLast"),

        };
    }

    @ParameterizedTest
    @MethodSource("removeLastTestArgs")
    void removeLastTest(String[]condition, String expected) {
        cut.memoryCell = condition;
        String actual = cut.removeLast();
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] saveTestArgs(){
        return new Arguments[]{
                Arguments.arguments(firstCase, "fire", false),
                Arguments.arguments(secondCase, "topper",true),
                Arguments.arguments(thirdCase, "pine",true),
                Arguments.arguments(fourthCase, "unbearable", false),
                Arguments.arguments(fifthCase, "fae",true),

        };
    }

    @ParameterizedTest
    @MethodSource("saveTestArgs")
    void saveTest(String[] condition,String wordToSafe,boolean expected) {
        cut.memoryCell = condition;
        boolean actual = cut.save(wordToSafe);
        Assertions.assertEquals(expected,actual);

    }

    static Arguments[] getMemoryInfoTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"one", "two", "three", "four", "five"}, new MemoryInfo (5,0)),
                Arguments.arguments(new String[]{null, "one", "two", "three", null}, new MemoryInfo (5,40)),
                Arguments.arguments(new String[]{null, null, "one", "two"}, new MemoryInfo (4,50)),
                Arguments.arguments(new String[]{}, new MemoryInfo (0,0)),
                Arguments.arguments(new String[]{null, null, null, null, null}, new MemoryInfo (5,100)),

        };
    }

    @ParameterizedTest
    @MethodSource("getMemoryInfoTestArgs")
    void getMemoryInfoTest(String[]condition, MemoryInfo expected) {
        cut.memoryCell = condition;
        MemoryInfo actual = cut.getMemoryInfo();
        Assertions.assertEquals(expected,actual);

    }
}