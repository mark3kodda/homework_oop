package test.com.oophomework.models;

import main.com.oophomework.models.Device;
import main.com.oophomework.models.Memory;
import main.com.oophomework.models.ProcessorArm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class DeviceTest {

    private final Memory memory = new Memory(5);
    private final ProcessorArm proc = new ProcessorArm(5000,10,32);
    private final Device cut = new Device(proc,memory);

    static Arguments[] readAllTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"one", "two", "three", "four", "five"}, new String[]{ "five","four","three","two","one"}),
                Arguments.arguments(new String[]{null, "one", "two", "three", null}, new String[]{"three","two","one"}),
                Arguments.arguments(new String[]{null, null, "one", "two"}, new String[]{"two","one"}),
                Arguments.arguments(new String[]{}, new String[]{}),
                Arguments.arguments(new String[]{null, null, null, null, null}, new String[]{}),

        };
    }

    @ParameterizedTest
    @MethodSource("readAllTestArgs")
    void readAll(String[]condition,String [] expected) {
        cut.save(condition);
        String [] actual = cut.readAll();
        Assertions.assertArrayEquals(expected,actual);
    }

}